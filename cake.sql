-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 10, 2018 at 06:00 AM
-- Server version: 5.7.17-log
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `cake`
--

CREATE TABLE `cake` (
  `cakename` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `content` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `teast` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `price` int(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cake`
--

INSERT INTO `cake` (`cakename`, `content`, `teast`, `price`) VALUES
('DoubleChocolateFudge', 'ที่สุดแห่งความเนียนนุ่มของเนื้อเค้กช็อกโกแลต  ผสานรสชาติละมุน นุ่มลิ้นของช็อกโกแลตฟัดจ์ เนื้อชิฟฟ่อน สลับชั้นครีมช็อกโกแลต 3 ชั้น ประดับด้วยเชอร์รี่สีแดงเชื่อมเข้ากันอย่างลงตัว', 'ช็อกโกแลต', 400),
('BlackForest', 'เค้กเนื้อสปองจ์ช็อกโกแลตแท้  สลับชั้นครีม 3ชั้น หนา นุ่ม หวาน หอม  ประดับตกแต่งหน้าเค้กด้วยครีมสด  และ  เชอร์รี่เชื่อมสีสันสวยงามโรยรอบข้างตัวเค้กด้วยเกล็ดช็อกโกแลต อย่างลงตัวในแบบฉบับยุโรปแท้', 'ช็อกโกแลต', 500),
('Genoa', 'เนื้อเค้กสปองจ์เนียนนุ่มสลับชั้นครีม 3 ชั้น โรยหน้าเค้กด้วยอัลมอนด์เคลือบคาราเมลอบกรอบจากแคลิฟอร์เนีย  สัมผัสถึงความกรอบมันหวานฉ่ำ ละมุนทุกคำที่ลิ้มลอง', 'กาแฟ', 400),
('MilkWhiteChoc', 'สำหรับท่านที่ชื่นชอบเค้กนม ขอนำเสนอเค้กรสชาติใหม่เนื้อเค้กสปองจ์นมสดแท้ สลับชั้นครีมนมสด  3  ชั้น หวาน  หอม ละมุนทุกคำที่ลิ้มลอง ตกแต่งหน้าเค้กด้วยไวท์ช็อกโกแลตขูดฝอยและไวท์ช็อกโกแลตแผ่น', 'วนิลา', 500),
('SmootyOrangeFudge', 'สัมผัสเค้กเนื้อชิฟฟ่อนราดด้วยซอสส้มเนื้อเนียนนุ่ม  สลับชั้นซอสส้ม 3 ชั้น ราดหน้าเค้กด้วยซอสส้มหวานซ่อนเปรี้ยว พร้อมประดับประดาด้วยชิ้นส้ม  กีวี และผลเชอรี่ เคลือบเจลาตินที่เข้ากันอย่างลงตัว', 'ส้ม', 400),
('ChewyStawberry', 'สัมผัสรสชาติของเค้กผลไม้เนื้อบัตเตอร์สลับชั้นครีม 4  ชั้น ที่มีส่วนผสมของแยมสตรอเบอร์รี่และระหว่างชั้นครีมมีผลไม้ตระกูลเบอร์รี่ต่าง ๆ ทั้งบลูเบอร์รี่ ราสเบอร์รี่ แคลนเบอร์รี่ และบิงเชอร์รี่ รสหวานซ่อนเปรี้ยว ประดับตกแต่งหน้าเค้กด้วยแยมสตรอเบอร์รี่ฉ่ำ', 'วนิลา', 500);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cake`
--
ALTER TABLE `cake`
  ADD PRIMARY KEY (`cakename`),
  ADD UNIQUE KEY `teast` (`cakename`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
