<?php
ob_start();
session_start();
require __DIR__ . '/vendor/autoload.php';
use Firebase\JWT\JWT;

if($_COOKIE["jwt"] ==""){

  header("Location: login.php");
  
  }

?>
<!DOCTYPE html>
<html>

<head>
<title>Tu-cake Page1</title>
<script src="https://d3oukl23643fvj.cloudfront.net/JsNo1.js"></script>
<link href="https://d3oukl23643fvj.cloudfront.net/CssNo2.css" rel="stylesheet">
<link href="https://d3oukl23643fvj.cloudfront.net/CssNo1.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Kanit&amp;subset=thai" rel="stylesheet">
<meta charset="utf-8">
</script>
</head>
<body>﻿

  <div class="rig">

  <br><br><a class="log"href="logout.php">ออกจากระบบ</a>  <br><br>
  </div>

  <div class="top">
  <a class="logo" href="MainCake.php"><img src="https://d3oukl23643fvj.cloudfront.net/logo.png"width="200" height="200" ></img></a>

  <ul class="active">
  <li><a href="MainCake.php">หน้าแรก</a></li>
  <li><a href="MenuCake.php">เค้ก</a></li>
  <li><a href="Promotion.php">โปรโมชั่น</a></li>
  <li><a href="HowTobuy.php">วิธีการสั่งซื้อ</a></li>
  <li><a href="Contact.php">ติดต่อเรา</a></li>
  <li><a href="dashBoard.html">สมาชิก</a></li>
</ul>
</div>

  <div class="scroll">

<!--list-->
<section class="a"><div class="list">



<!-- showMenu -->

<p class="H1">กรุณาเลือกสินค้าที่ต้องการ</p>
<div class="allCake">
<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/7.png','เค้กบัตเตอร์สลับชั้นครีม 4 ชั้น ครีมมีส่วนผสมของแยมบลูเบอร์รี่ และระหว่างชั้นครีมมีผลไม้ตระกูลเบอร์รี่ต่าง ๆ ทั้งบลูเบอร์รี่ ราสเบอร์รี่  แคลนเบอร์รี่  และบิงเชอร์รี่  แต่งหน้าเค้กหวานฉ่ำด้วยแยมบลูเบอร์รี่ รสหวานซ่อนเปรี้ยว   ประดับประดาหน้าเค้กไปด้วย เชอร์รี่เชื่อมสีสดใส และกีวี่ ดูเข้ากันอย่างลงตัว')" src="https://d3oukl23643fvj.cloudfront.net/CAKE/7.png" alt=""/ width="600" height="400"  />
  </a>
  <div class="name">Berry Mix</div>
</div>

<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/8.png','เค้กบัตเตอร์ช็อกโกแลต เนื้อแน่น    สลับชั้นครีม 3 ชั้นหน้าเค้กเคลือบช็อกโกแลตแข็ง และประดับตกแต่งด้วยช็อกบอลรสชาติกลมกล่อม วาดลวดลายด้วยไวท์ช็อกโกแลตดูสวยงาม')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/8.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Paradiso Carola</div>
</div>

<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/9.0.png','ลิ้มลองความอร่อยกับบัตเตอร์เค้กเนื้อแน่นสลับชั้นครีม 3 ชั้น รสเนียนนุ่ม ประดับประดาหน้าเค้กด้วยไวท์ช็อกโกแลต และครีมคาราเมล หวาน หอม  มัน ทุกคำที่สัมผัส ')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/9.0.png" alt="" width="600" height="400" />
  </a>
  <div class="name">White Pearl Butter</div>
</div>

<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/10.png','เค้กที่ผสมผสานความลงตัวระหว่างขนมตะวันตกและขนมไทย เป็นเค้กพิเศษที่มีเฉพาะช่วงเดือนเมษายน - พฤษภาคมเท่านั้น')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/10.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Foy Thong</div>
</div>


<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/11.png','เค้กเนื้อบัตเตอร์ดาร์กช็อกโกแลต  เนื้อแน่น สลับชั้นซอสช็อกโกแลตเข้มข้น 3 ชั้น  หวานฉ่ำ  กลมกล่อม  สัมผัสได้ถึงรสชาติของช็อกโกแลตอย่างแท้จริง  แต่งหน้าเค้กด้วยครีมดาร์กช็อกโกแลตและผงโกโก้โรยรอบตัวเค้กด้วยช็อกโกแลตชิพรสเข้ม')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/11.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Premium Dark Chocolate</div>
</div>

<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/12.png','เค้กมอคค่าสูตรพิเศษเฉพาะ สลับชั้นครีม  3 ชั้น   ตำรับอิตาเลียนแท้ ราดหน้าด้วยคาราเมลมอคค่าหวานจับใจ และตกแต่งด้วยอัลมอนด์สดแท้จากแคลิฟอร์เนีย ให้ทุกคำอร่อยได้อย่างลงตัว')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/12.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Almond Mocha</div>
</div>

<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/13.png','เค้กสปองจ์  เนื้อเค้กผสมผสานรสชาติของใบเตย สลับชั้นครีมผสมมะพร้าว 3  ชั้น หอม หวาน มัน แต่งหน้าเค้กด้วยวุ้นมะพร้าวอ่อน รสกลมกล่อม ประดับประดารอบตัวเค้กด้วยครีมมะพร้าวและมะพร้าวอ่อนขูดฝอยเข้ากันอย่างลงตัว')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/13.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Freshen Coco</div>
</div>



<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/1.png','เนื้อเค้กสปองจ์เนียนนุ่มสลับชั้นครีม 3 ชั้น โรยหน้าเค้กด้วยอัลมอนด์เคลือบคาราเมลอบกรอบจากแคลิฟอร์เนีย  สัมผัสถึงความกรอบมันหวานฉ่ำ ละมุนทุกคำที่ลิ้มลอง')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/1.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Genoa</div>
</div>


<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/2.png','สัมผัสรสชาติของเค้กผลไม้เนื้อบัตเตอร์สลับชั้นครีม 4  ชั้น ที่มีส่วนผสมของแยมสตรอเบอร์รี่และระหว่างชั้นครีมมีผลไม้ตระกูลเบอร์รี่ต่าง ๆ ทั้งบลูเบอร์รี่ ราสเบอร์รี่ แคลนเบอร์รี่ และบิงเชอร์รี่ รสหวานซ่อนเปรี้ยว ประดับตกแต่งหน้าเค้กด้วยแยมสตรอเบอร์รี่ฉ่ำ  ผลเชอร์รี่ และกีวี่ อย่างสวยงามและหน้ารับประทาน')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/2.png" alt="" width="600" height="400"/>
  </a>
  <div class="name">Chewy Strawberry</div>
</div>

<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/3.png','ที่สุดแห่งความเนียนนุ่มของเนื้อเค้กช็อกโกแลต  ผสานรสชาติละมุน นุ่มลิ้นของช็อกโกแลตฟัดจ์ เนื้อชิฟฟ่อน สลับชั้นครีมช็อกโกแลต 3 ชั้น ประดับด้วยเชอร์รี่สีแดงเชื่อมเข้ากันอย่างลงตัว')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/3.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Double Chocolate Fudge</div>
</div>

<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/6.png','สัมผัสเค้กเนื้อชิฟฟ่อนราดด้วยซอสส้มเนื้อเนียนนุ่ม  สลับชั้นซอสส้ม 3 ชั้น ราดหน้าเค้กด้วยซอสส้มหวานซ่อนเปรี้ยว พร้อมประดับประดาด้วยชิ้นส้ม  กีวี และผลเชอรี่ เคลือบเจลาตินที่เข้ากันอย่างลงตัว')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/6.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Smooty Orange Fudge</div>
</div>
<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/15.png','สัมผัสรสชาติความอร่อยกับเค้กเนื้อชิฟฟ่อน เนื้อนุ่มละมุนสลับชั้นครีมสังขยาและแต่งหน้าด้วยสังขยาหอมหวาน กลมกล่อมกำลังดี  พร้อมประดับประดาด้วยไวท์ช็อกโกแลตและเชอร์รี่ สัมผัสถึงความเป็นไทยได้เป็นอย่างดี')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/15.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Smooty Pancake</div>
</div>
<div class="gallery">
    <img onclick= "myClick('https://d3oukl23643fvj.cloudfront.net/CAKE/17.png','สัมผัสรสชาติความหอมอร่อยเพื่อสุขภาพกับเค้กเนื้อสูตรเฉพาะ ผสมลูกพรุนแท้และความหอมหวานของลูกเกด ช็อกโกแลตชิฟ เพิ่มรสชาติเปรี้ยวอมหวานของผิวส้ม พร้อมเนื้อครีมหวานละมุนที่ผสมเนื้อลูกพรุน คุณภาพเยี่ยม และตบแต่งขอบเค้กด้วยแผ่นช็อกโกแลต')"src="https://d3oukl23643fvj.cloudfront.net/CAKE/17.png" alt="" width="600" height="400" />
  </a>
  <div class="name">Cake pun</div>
</div>
</div>
</div></section>



          <!--order-->
        <form  method="POST" action='buyer.php'>
        <section class="Show">
          <div class="cakeBackground">
          <div class="weightCake">
          <div class="showPic">
            <img name"mymy" id="myImage" src="https://d3oukl23643fvj.cloudfront.net/CAKE/7.png" value="document.getElementById('myImage').src">
            <input id="myImage1" type='hidden' name='cakeName1' value="document.getElementById('myImage').src">
          </div>




          <div class="Order">
            <p class="nameCC"id ="nameC" > เค้กบัตเตอร์สลับชั้นครีม 4 ชั้น ครีมมีส่วนผสมของแยมบลูเบอร์รี่ และระหว่างชั้นครีมมีผลไม้ตระกูลเบอร์รี่ต่าง ๆ ทั้งบลูเบอร์รี่ ราสเบอร์รี่  แคลนเบอร์รี่  และบิงเชอร์รี่  แต่งหน้าเค้กหวานฉ่ำด้วยแยมบลูเบอร์รี่ รสหวานซ่อนเปรี้ยว   ประดับประดาหน้าเค้กไปด้วย เชอร์รี่เชื่อมสีสดใส และกีวี่ ดูเข้ากันอย่างลงตัว

            </p>

            <p>จำนวน :<span> </span>
              <input  value="0" name="quantity" class="amount3" onChange="document.getElementById('demo').innerHTML = this.value*400;">

              <span class="tab"> กล่อง </span>
              </p>

            <p>รสชาติ :<span> </span>

         <input id="test" name='test' value="รสที่อยากได้">


          </p>
          </select></p>
            <p>ราคา  :<span> </span><button id="demo"class="bt0" value="document.getElementById('demo').value" >400 </button> <span class="tab">  บาท<span></p><br>
            <!--button  class="bt">สั่งซื้อ </button-->
            <input type="submit" value="สั่งซื้อ" class="submit">

          </p>

          </div>
        </div>
        </div>
      </session>
    </form>
<div class="last">
</div>
</body>
<html>
